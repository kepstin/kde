Title: kde/zeroconf-ioslave has been renamed to kde/kio-zeroconf
Author: Heiko Becker <heirecka@exherbo.org>
Content-Type: text/plain
Posted: 2022-08-17
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: kde/zeroconf-ioslave

Please install kde/kio-zeroconf and *afterwards* uninstall kde/zeroconf-ioslave.

1. Take note of any packages depending on kde/zeroconf-ioslave
cave resolve \!kde/zeroconf-ioslave

2. Install kde/kio-zeroconf:
cave resolve kde/kio-zeroconf -x

3. Re-install the packages from step 1.

4. Uninstall kde/zeroconf-ioslave
cave resolve \!kde/zeroconf-ioslave -x

Do it in *this* order or you'll potentially *break* your system.
