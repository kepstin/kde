Title: Changes with KDE Frameworks 5.63.0
Author: Heiko Becker <heirecka@exherbo.org>
Content-Type: text/plain
Posted: 2019-10-09
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: kde-frameworks/kcalcore
Display-If-Installed: kde-frameworks/kcontacts[>=15.08]

kde-frameworks/kcalcore and kde-frameworks/kcontacts were moved from KDE
Applications to KDE Frameworks, which use a different versioning scheme,
resulting in the need to forcefully downgrade kcontats (e.g. from 19.08.1
to 5.63.0). You can do that via:

# cave resolve -1 kde-frameworks/kcontacts --permit-downgrade kde-frameworks/kcontacts

For kcalcore that step is not really necessary because it was also renamed from
kde-frameworks/kcalcore to kcalendarcore. To deal with this please install
kde-frameworks/kcalendarcore and *afterwards* uninstall kde-frameworks/kcalcore:

1. Take note of any packages depending on kde-framworks/kcalcore:
cave resolve \!kde-frameworks/kcalcore

2. Install kde-frameworks/kcalendarcore:
cave resolve kde-frameworks/kcalendarcore -x1

3. Re-install the packages from step 1.

4. Uninstall kde-frameworks/kcalcore
cave resolve \!kde-frameworks/kcalcore -x

Do it in *this* order or you'll potentially *break* your system.

