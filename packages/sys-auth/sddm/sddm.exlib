# Copyright 2014 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

GITHUB_BRANCH="develop"

require github [ release=v${PV} suffix=tar.xz ] cmake
require 66-service openrc-service

export_exlib_phases src_install

SUMMARY="SDDM is a modern display manager based on Qt using technologies like QtQuick"
DESCRIPTION="
SDDM is extremely themeable. We put no restrictions on the user interface design,
it is completely up to the designer. We simply provide a few callbacks to the user
interface which can be used for authentication, suspend etc.
"

LICENCES="
    GPL-2
    ( CCPL-Attribution-3.0 CCPL-Attribution-ShareAlike-3.0 LGPL-3 ) [[ note = themes ]]
"
SLOT="0"
MYOPTIONS="
    journald [[ description = [ Log to systemd's journal ] ]]
    ( providers: elogind systemd ) [[
        *description = [ Session tracking provider ]
        number-selected = exactly-one
    ]]
"

QT_MIN_VER="5.8.0"

DEPENDENCIES="
    build:
        dev-python/docutils [[ note = [ man pages ] ]]
        kde-frameworks/extra-cmake-modules[>=1.4.0]
        virtual/pkg-config
    build+run:
        sys-libs/pam [[ note = [ Optional ] ]]
        user/sddm
        x11-libs/libxcb
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qttools:5[>=${QT_MIN_VER}] [[ note = [ Qt5LinguistTools ] ]]
        providers:elogind? ( sys-auth/elogind )
        providers:systemd? ( sys-apps/systemd )
    run:
        x11-apps/xauth
        x11-server/xorg-server
    suggestion:
        sys-apps/upower  [[ description = [ Used for suspend support ] ]]
        sys-libs/wayland [[ description = [ Needed to start a wayland desktop session ] ]]
        x11-libs/qtvirtualkeyboard:5[>=${QT_MIN_VER}] [[
            description = [ Input method useful for touchscreen devices ]
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_MAN_PAGES:BOOL=TRUE
    -DDBUS_CONFIG_DIR:PATH=/usr/share/dbus-1/system.d
    -DRUNTIME_DIR:PATH=/run/${PN}
    -DENABLE_PAM:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTIONS=( '!providers:systemd NO_SYSTEMD' )
CMAKE_SRC_CONFIGURE_OPTION_ENABLES=( JOURNALD )
CMAKE_SRC_CONFIGURE_OPTION_USES=( 'providers:elogind ELOGIND' )

sddm_src_install() {
    default

    keepdir /etc/${PN}.conf.d

    # Needed at run-time
    keepdir /var/lib/${PN}
    edo chown -R sddm "${IMAGE}"/var/lib/${PN}

    install_66_files
    install_openrc_files
}

