# Copyright 2016-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf5_min_ver
myexparam qt_min_ver

require kde-apps kde [ translations='ki18n' ]
require xdummy [ phase=test ]

export_exlib_phases src_test

SUMMARY="Library for common KDE PIM applications"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS="
    designer [[ description = [ Install Qt Designer plugins ] ]]
    doc [[ description = [ Build API docs in Qt's help format (.qch) ] ]]
"

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        doc? (
            app-doc/doxygen
            x11-libs/qttools:5   [[ note = qhelpgenerator ]]
        )
    build+run:
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        designer? ( x11-libs/qttools:5[>=${QT_MIN_VER}] )
"


CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=(
    'designer DESIGNERPLUGIN'
    'doc QCH'
)

libkdepim_src_test() {
    xdummy_start

    default

    xdummy_stop
}

