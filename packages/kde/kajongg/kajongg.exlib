# Copyright 2013, 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf5_min_ver
myexparam qt_min_ver

# No multibuild because it installs its python files into /usr/share
require kde-apps kde python [ blacklist='2' multibuild=false ]
require gtk-icon-cache

export_exlib_phases src_install

SUMMARY="Classic Mah Jongg for four players"
DESCRIPTION="
Kajongg is the ancient Chinese board game for 4 players. Kajongg can be used in two different ways:
Scoring a manual game where you play as always and use Kajongg for the computation of scores and
for bookkeeping. Or you can use Kajongg to play against any combination of other human players or
computer players."
HOMEPAGE+=" https://apps.kde.org/${PN}/"

LICENCES="GPL-2 FDL-1.2 BSD-3"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        dev-db/sqlite:3
        dev-python/PyQt5[python_abis:*(-)?]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        net-twisted/Twisted[>=16.6.0][python_abis:*(-)?]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
    run:
        kde/libkmahjongg:5
    recommendation:
        media-sound/vorbis-tools[ogg123] [[
            description = [ Tiles are announced by different voices per player ]
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DPYTHON_EXECUTABLE:PATH=${PYTHON}
)

kajongg_src_install() {
    kde_src_install

    edo chmod +x "${IMAGE}"/usr/share/${PN}/${PN}{,server}.py

    edo mkdir -p "${IMAGE}"/usr/$(exhost --target)/bin
    dosym /usr/share/${PN}/${PN}.py /usr/$(exhost --target)/bin/${PN}
    dosym /usr/share/${PN}/${PN}server.py /usr/$(exhost --target)/bin/${PN}server
}

