# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde

myexparam kf5_min_ver
myexparam qt_min_ver

SUMMARY="A pure QtQuick/Kirigami Qt Quick Controls Style"
DESCRIPTION="
Unlike qqc2-desktop-style it doesn't depend on QtWidgets and is mainly
interesting for mobile devices."

LICENCES="
    LGPL-2
    || ( LGPL-2.1 LGPL-3 )
    || ( LGPL-3 GPL-2 )
"
SLOT="0"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
    run:
        x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
"

DEFAULT_SRC_CONFIGURE_PARAMS+=(
    # Isn't actually used, also pulls in qtx11extras, apparently because of
    # problems with static linking, which we don't do.
    -DCMAKE_DISABLE_FIND_PACKAGE_X11:BOOL=TRUE
)

