# Copyright 2016-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf5_min_ver
myexparam qt_min_ver

require kde-apps kde [ translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test

SUMMARY="A library providing crypto for E-Mails"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS="
    doc [[ description = [ Build API docs in Qt's help format (.qch) ] ]]
"

GPGME_MIN_VER=1.15.0
exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        doc? (
            app-doc/doxygen
            x11-libs/qttools:5   [[ note = qhelpgenerator ]]
        )
    build+run:
        app-crypt/gpgme[>=${GPGME_MIN_VER}]
        app-crypt/libqgpgme[>=${GPGME_MIN_VER}]
        dev-libs/boost[>=1.34.0]
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpimtextedit:5[>=${PV}]   [[ note = [ could be optional ] ]]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
"

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc QCH' )

DEFAULT_SRC_TEST_PARAMS+=( ARGS+="-E newkeyapprovaldialogtest" )

libkleo_src_test() {
    xdummy_start

    esandbox allow_net "unix:${TEMP}/keyresolvercoretest-*/S.gpg-agent*"
    esandbox allow_net --connect "unix:${TEMP}/keyresolvercoretest-*/S.gpg-agent*"

    test-dbus-daemon_run-tests

    esandbox disallow_net --connect "unix:${TEMP}/keyresolvercoretest-*/S.gpg-agent*"
    esandbox disallow_net "unix:${TEMP}/keyresolvercoretest-*/S.gpg-agent*"

    xdummy_stop
}

