# Copyright 2013, 2015-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf5_min_ver
myexparam qt_min_ver

require kde-apps kde [ translations='ki18n' ] gtk-icon-cache

SUMMARY="A simple game of evading killer robots"
DESCRIPTION="
Who created the robots and why they have been programmed to destroy, no one knows. All that is known
is that the robots are numerous and their sole objective is to destroy you. Fortunately for you,
their creator has focused on quantity rather than quality and as a result the robots are severely
lacking in intelligence. Your superior wit and a fancy teleportation device are your only weapons
against the never-ending stream of mindless automatons."
HOMEPAGE+=" https://apps.kde.org/${PN}/"

LICENCES="GPL-2 FDL-1.2"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES+="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde/libkdegames:5[>=21.03.80]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
"

# CMakeLists.txt: "#add_subdirectory(tests)"
#        kde-frameworks/kdelibs4support:5[>=${KF5_MIN_VER}]

