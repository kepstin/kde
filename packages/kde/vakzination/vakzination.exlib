# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf5_min_ver
myexparam qt_min_ver

require plasma-mobile kde
require xdummy [ phase=test ]

export_exlib_phases src_test

SUMMARY="Manages your health certificates like vaccination, test, and recovery certificates"
DESCRIPTION="
The following certificate formats are currently supported:

* Indian vaccination certificates (based on
  https://en.wikipedia.org/wiki/Verifiable_credentials)
* EU \"Digitial Green Certificate\" (DCG) vaccination, test and recovery
  certificates (see https://github.com/eu-digital-green-certificates)
"

LICENCES="
    Apache-2.0      [[ note = [ test data ] ]]
    BSD-3           [[ note = [ cmake, gradle ] ]]
    CC0             [[ note = [ desktop files, README, etc ] ]]
    CC-PublicDomain [[ note = [ src/expected.h ] ]]
    FSFAP           [[ note = [ appstream metadata ] ]]
    || ( GPL-2 GPL-3 )
"
SLOT="0"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
    build+run:
        kde/khealthcertificate
        kde/kitinerary[>=21.08.0]   [[ note = [ aka 5.18.0 ] ]]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
    run:
        kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
        kde-frameworks/prison:5[>=${KF5_MIN_VER}]
"

# Fails at upstream CI too, maybe a outdate test cert?
DEFAULT_SRC_TEST_PARAMS+=( ARGS+="-E certificatesmodeltest" )

vakzination_src_test() {
    xdummy_start

    default

    xdummy_stop
}

