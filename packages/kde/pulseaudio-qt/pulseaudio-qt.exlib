# Copyright 2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ subdir=${PN} ] kde
require alternatives

export_exlib_phases src_install

SUMMARY="Qt bindings for libpulse"

LICENCES="LGPL-2.1"
MYOPTIONS="doc"

if ever at_least 1.3 ; then
    QT_MIN_VER="5.15.0"
else
    QT_MIN_VER="5.10.0"
fi

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? (
            app-doc/doxygen
            x11-libs/qttools:5
        )
    build+run:
        dev-libs/glib:2
        media-sound/pulseaudio[>=5.0.0]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
    build+test:
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
    run:
        !kde/pulseaudio-qt[<1.2-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc QCH' )

CMAKE_SRC_CONFIGURE_TESTS+=(
    '-DCMAKE_DISABLE_FIND_PACKAGE_Qt5Quick:BOOL=FALSE -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Quick:BOOL=TRUE'
    '-DCMAKE_DISABLE_FIND_PACKAGE_Qt5Qml:BOOL=FALSE -DCMAKE_DISABLE_FIND_PACKAGE_Qt5Qml:BOOL=TRUE'
)

pulseaudio-qt_src_install() {
    local arch_dependent_alternatives=() other_alternatives=()
    host=$(exhost --target)

    kde_src_install

    arch_dependent_alternatives+=(
        /usr/${host}/include/KF5/KF5PulseAudioQt KF5PulseAudioQt-${SLOT}
        /usr/${host}/include/KF5/pulseaudioqt_version.h pulseaudioqt_version-${SLOT}.h
        /usr/${host}/lib/cmake/KF5PulseAudioQt KF5PulseAudioQt-${SLOT}
        /usr/${host}/lib/libKF5PulseAudioQt.so libKF5PulseAudioQt-${SLOT}.so
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"

    if option doc ; then
        other_alternatives+=(
            /usr/share/doc/qt5/KF5PulseAudioQt.qch  KF5PulseAudioQt-${SLOT}.qch
            /usr/share/doc/qt5/KF5PulseAudioQt.tags KF5PulseAudioQt-${SLOT}.tags
        )
        alternatives_for _${PN} ${SLOT} ${SLOT} "${other_alternatives[@]}"
    fi
}

