# Copyright 2014-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
exparam -v major_version kf_major_version

require kde-frameworks kde [ kf_major_version=${major_version} translations='qt' ]
require freedesktop-mime

export_exlib_phases src_prepare src_test src_install

SUMMARY="Addons to QtCore"
DESCRIPTION="
They perform various tasks such as manipulating mime types, autosaving files,
creating backup files, generating random sequences, performing text
manipulations such as macro replacement, accessing user information and many
more."

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] LGPL-2 LGPL-2.1"
MYOPTIONS="
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        x11-misc/shared-mime-info[>=1.8]
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd[>=167] )
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # Needed for migrating settings from ~/.kde4 to ~/.config
    -D_KDE4_DEFAULT_HOME_POSTFIX=4
    # Disable deprecated/obsolete FAM support in favor of using inotify directly
    -DCMAKE_DISABLE_FIND_PACKAGE_FAM:BOOL=TRUE
)

kcoreaddons_src_prepare() {
    kde_src_prepare

    # https://bugs.kde.org/show_bug.cgi?id=339117
    edo sed -e '/kshelltest/d' \
            -i autotests/CMakeLists.txt

    # Disable two tests which fail as of 5.44.0
    edo sed -e '/set(KDIRWATCH_BACKENDS_TO_TEST/ s/Stat//' \
            -e 's/HAVE_QFILESYSTEMWATCHER/false/' \
            -i autotests/CMakeLists.txt
}

kcoreaddons_src_test() {
    esandbox allow_net 'unix:/tmp/fam-paludisbuild/fam-*'
    default
    esandbox disallow_net 'unix:/tmp/fam-paludisbuild/fam-*'
}

kcoreaddons_src_install() {
    kde_src_install

    if [[ ${major_version} == 6 ]] ; then
        # Just don't install this for co-installability at the moment
        edo rm "${IMAGE}"/usr/share/mime/packages/kde5.xml \
            "${IMAGE}"/usr/$(exhost --target)/bin/desktoptojson
        edo rmdir "${IMAGE}"/usr/share/{mime/packages,mime} \
            "${IMAGE}"/usr/$(exhost --target)/bin
    fi
}

