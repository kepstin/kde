# Copyright 2014 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
exparam -v major_version kf_major_version

require kde-frameworks kde [ kf_major_version=${major_version} translations='ki18n' ]

SUMMARY="KDE Gettext-based UI text internationalization"
DESCRIPTION="
KI18n provides functionality for internationalizing user interface text
in applications, based on the GNU Gettext translation system.
It wraps the standard Gettext functionality, so that the programmers
and translators can use the familiar Gettext tools and workflows.

KI18n provides additional functionality as well, for both programmers
and translators, which can help to achieve a higher overall quality
of source and translated text. This includes argument capturing,
customizable markup, and translation scripting.
"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] LGPL-2.1"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-lang/python:*[>=2.6]
        sys-devel/gettext
    build+test:
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
    run:
        app-text/iso-codes
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # Scripted translations, recommended, but possibly optional
    -DBUILD_WITH_QML=TRUE
)

if [[ $(exhost --target) == *-musl* ]] ; then
    # klocalizedstringtest failure is musl specific, the others need fr_CH
    DEFAULT_SRC_TEST_PARAMS+=(
        ARGS+=" -E '(klocalizedstringtest|kcatalogtest|kcountrytest|kcountrysubdivisiontest)'"
    )
else
    # They need fr_CH
    DEFAULT_SRC_TEST_PARAMS+=(
        ARGS+=" -E '(kcatalogtest|kcountrytest|kcountrysubdivisiontest)'"
    )
fi

