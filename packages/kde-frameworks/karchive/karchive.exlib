# Copyright 2014-2021 Heiko Becker <heirecka@exherbo.org>
# Copyright 2021 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
exparam -v major_version kf_major_version

require kde-frameworks kde [ kf_major_version=${major_version} translations='qt' ]
require utf8-locale

export_exlib_phases src_test pkg_setup

SUMMARY="Qt 5 addon providing access to numerous types of archives"
DESCRIPTION="
KArchive provides classes for easy reading, creation and manipulation of
\"archive\" formats like ZIP and TAR.

If also provides transparent compression and decompression of data, like the
GZip format, via a subclass of QIODevice."


LICENCES="GPL-2 LGPL-2.1"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-arch/bzip2
        app-arch/xz
        app-arch/zstd [[ note = [ Only controllable via find_package(PkgConfig) ] ]]
        sys-libs/zlib
"

karchive_src_test() {
    # Some tests attempt to write to /I/dont/exist and expect no such file or
    # directory rather than permission denied.
    [[ -e /I ]] && die 'tests require allowing access to /I'
    esandbox allow /I

    default
}

karchive_pkg_setup() {
    require_utf8_locale
}

