# Copyright 2014 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2020-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
exparam -v major_version kf_major_version

require kde-frameworks kde [ kf_major_version=${major_version} translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test src_install

SUMMARY="KDE text editor component"

DESCRIPTION="
KTextEditor provides a powerful text editor component that you can embed in your
application, either as a KPart or using the KF5::TextEditor library (if you need
more control).

The text editor component contains many useful features, from syntax
highlighting and automatic indentation to advanced scripting support, making it
suitable for everything from a simple embedded text-file editor to an advanced IDE.
"

LICENCES="
    BSD-2 [[ note = [ cmake scripts ] ]]
    LGPL-2.1
"
MYOPTIONS="
    editorconfig [[
        description = [ Configure consistent coding styles between different editors ]
    ]]
"

DEPENDENCIES="
    build+run:
        kde-frameworks/karchive:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kauth:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/sonnet:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/syntax-highlighting:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
        editorconfig? ( app-text/editorconfig )
    recommendation:
        dev-scm/git [[
            description = [ Add extra functionality when editing files in git repos ]
        ]]
"

if [[ ${major_version} == 6 ]] ; then
    DEPENDENCIES+="
        build+run:
            x11-libs/qt5compat:6[>=${QT_MIN_VER}]
    "
fi

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DENABLE_KAUTH:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    EditorConfig
)

# Skip failing tests
DEFAULT_SRC_TEST_PARAMS+=( ARGS+="-E '(vimode_modes|vimode_keys)'" )

ktexteditor_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests

    xdummy_stop
}

ktexteditor_src_install() {
    kde_src_install

    if [[ ${major_version} == 6 ]] ; then
        # Just don't install this for co-installability at the moment
        edo rm "${IMAGE}"/usr/share/dbus-1/system-services/org.kde.ktexteditor.katetextbuffer.service \
            "${IMAGE}"/usr/share/dbus-1/system.d/org.kde.ktexteditor.katetextbuffer.conf \
            "${IMAGE}"/usr/share/katepart5/script/README.md \
            "${IMAGE}"/usr/share/kdevappwizard/templates/ktexteditor-plugin.tar.bz2 \
            "${IMAGE}"/usr/share/polkit-1/actions/org.kde.ktexteditor.katetextbuffer.policy \
            "${IMAGE}"/usr/$(exhost --target)/libexec/kauth/kauth_ktexteditor_helper
        edo rmdir "${IMAGE}"/usr/share/{dbus-1/system-services,dbus-1/system.d,dbus-1} \
            "${IMAGE}"/usr/share/{katepart5/script,katepart5} \
            "${IMAGE}"/usr/share/{kdevappwizard/templates,kdevappwizard} \
            "${IMAGE}"/usr/share/{polkit-1/actions,polkit-1} \
            "${IMAGE}"/usr/$(exhost --target)/{libexec/kauth,libexec}
    fi
}

