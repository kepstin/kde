# Copyright 2013-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ subdir=frameworks/$(ever range -2) ] cmake

SUMMARY="Extra modules and scripts for CMake"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="doc"

ever at_least scm || \
UPSTREAM_RELEASE_NOTES="https://kde.org/announcements/frameworks/5/${PV}/"
UPSTREAM_DOCUMENTATION="https://api.kde.org/ecm/"

DEPENDENCIES+="
    build:
        doc? (
            dev-python/Sphinx[>=1.2]
            x11-libs/qttools:5 [[ note = [ QCollectionGenerator ] ]]
        )
    test:
        x11-libs/qtbase:5
        x11-libs/qttools:5 [[ note = [ Qt5Linguist ] ]]
    suggestion:
        dev-util/clazy [[ description = [ Richer warnings for Qt-related code with ENABLE_CLAZY ] ]]
        dev-util/reuse [[ description = [ For checking outbound licences ] ]]
"

CMAKE_SRC_CONFIGURE_OPTION_BUILDS=(
    'doc HTML_DOCS'
    'doc MAN_DOCS'
)
CMAKE_SRC_CONFIGURE_PARAMS=(
    -DDOC_INSTALL_DIR:PATH=/usr/share/doc/${PNVR}
    -DSHARE_INSTALL_DIR:PATH=/usr/share
    -DBUILD_QTHELP_DOCS:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_TESTS=( '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE' )

# KDEFetchTranslations: Can't possibly work with tarballs
# ECMPoQmToolsTest: Only fails in paludis env
DEFAULT_SRC_TEST_PARAMS+=( ARGS+="-E '(ECMPoQmToolsTest|KDEFetchTranslations)'" )

