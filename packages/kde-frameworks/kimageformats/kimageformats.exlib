# Copyright 2014-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
exparam -v major_version kf_major_version

require kde-frameworks [ docs=false ] kde [ kf_major_version=${major_version} ]
require xdummy [ phase=test ]

export_exlib_phases src_test

SUMMARY="Provides additional image format plugins for QtGui"
DESCRIPTION="
As such it is not required for the compilation of any other software, but may
be a runtime requirement for Qt-based software to support certain image
formats."

LICENCES="LGPL-2.1"
MYOPTIONS="
    avif [[ description = [ Support for the AV1 Image File Format ] ]]
    jpegxl [[ description = [ Support for the JPEG XL image file format ] ]]
    heif [[ description = [ Support for the H(igh) E(fficiency) I(mage) F(ile) F(ormat) ] ]]
    openexr
    raw
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        kde-frameworks/karchive:${major_version}[>=${KF5_MIN_VER}]
        avif? ( media-libs/libavif:=[>=0.8.2] )
        jpegxl? ( media-libs/libjxl[>=0.6.1] )
        heif? ( media-libs/libheif[>=1.10.0] )
        openexr? ( media-libs/openexr )
        raw? ( media-libs/libraw[>=0.20.2] )
    test:
        avif? (
            media-libs/libavif:=[>=0.8.2][providers:aom] [[
                note = [ the only codec supporting lossless, which is used by tests ]
            ]]
        )

    suggestion:
        x11-libs/qtimageformats:${major_version}[>=${QT_MIN_VER}] [[
            description = [ Provides additional plugins for JPEG-2000 and DDS ]
        ]]
"

CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'heif KIMAGEFORMATS_HEIF'
    'jpegxl KIMAGEFORMATS_JXL'
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'avif libavif'
    OpenEXR
    'raw LibRaw'
)

kimageformats_src_test()  {
    xdummy_start

    default

    xdummy_stop
}

